<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BukuModel extends Model
{
    protected $table = 'buku';
    protected $fillable = [
        'id_buku', 'nama_buku', 'stok', 'tahun_terbit', 'id_penerbit', 'nama_penerbit', 'is_active', 'created_at', 'updated_at',
    ];
}