<?php

namespace App\Http\Controllers;

use App\BukuModel;
use App\PenerbitModel;
use DB;
use Illuminate\Http\Request;

class BukuController extends Controller
{

    public function index()
    {
        $data = DB::table('buku')
            ->join('penerbit', 'buku.id_penerbit', '=', 'penerbit.id_penerbit')
            ->select('buku.id_buku', 'buku.nama_buku', 'buku.stok', 'buku.tahun_terbit', 'penerbit.id_penerbit', 'penerbit.nama_penerbit', 'buku.created_at', 'buku.updated_at')
            ->where('buku.is_active', '1')
            ->get();

        return view('admin.buku.index', compact('data'));

    }

    public function tambah_buku()
    {
        $p = PenerbitModel::all();
        $data = BukuModel::all();
        return view('admin.buku.tambah', compact('p', 'data'));
    }

    public function postData1(Request $request, BukuModel $BukuModel)
    {

        $simpan = $BukuModel->create([
            'nama_buku' => $request->nama_buku,
            'id_penerbit' => $request->id_penerbit,
            'stok' => $request->stok,
            'tahun_terbit' => $request->tahun_terbit,
            'is_active' => $request->is_active,
            'created_at' => $request->created_at,
            'updated_at' => now(),
        ]);

        if (!$simpan->exists) {
            return redirect()->route('tampil_buku')->with('error', 'data gagal disimpan');
        }
        return redirect()->route('tampil_buku')->with('success', 'data berhasil disimpan');
    }

    public function editData($id)
    {
        $p = PenerbitModel::all();
        $data = BukuModel::where('id_buku', $id)->first();
        return view('admin.buku.edit', compact('data', 'p'));
    }

    public function updateData($id, BukuModel $BukuModel, Request $request)
    {

        $simpan = $BukuModel->where('id_buku', $id)->update([
            'nama_buku' => $request->nama_buku,
            'stok' => $request->stok,
            'tahun_terbit' => $request->tahun_terbit,
            'id_penerbit' => $request->id_penerbit,
            'updated_at' => now(),
        ]);

        if (!$simpan) {
            return redirect()->route('tampil_buku')->with('error', 'data gagal di update');
        }

        return redirect()->route('tampil_buku')->with('success', 'data berhasil di update');
    }

    public function softDelete($id, BukuModel $BukuModel)
    {

        $simpan = $BukuModel->where('id_buku', $id)->update([
            'is_active' => '0',
        ]);

        if (!$simpan) {
            return redirect()->route('tampil_buku')->with('error', 'data gagal di dihapus');
        }

        return redirect()->route('tampil_buku')->with('success', 'data berhasil di hapus');
    }

    // FUNGSI CARI

    public function search()
    {
        $cari_buku = $_GET['cari'];

        $t = PenerbitModel::all();

        $data = BukuModel::where('nama_buku', 'LIKE', '%' . $cari_buku . '%')
            ->orwhere('nama_penerbit', 'LIKE', '%' . $cari_buku . '%')
            ->orwhere('id_buku', 'LIKE', '%' . $cari_buku . '%')
            ->join('penerbit', 'buku.id_penerbit', '=', 'penerbit.id_penerbit')
            ->select('buku.id_buku', 'buku.nama_buku', 'buku.stok', 'buku.tahun_terbit', 'penerbit.id_penerbit', 'penerbit.nama_penerbit', 'buku.created_at')
            ->where('buku.is_active', '1')
            ->get();

        return view('admin.buku.index', compact('data'));
    }
}