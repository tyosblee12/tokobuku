<?php

namespace App\Http\Controllers;

use App\PenerbitModel;
use DB;
use Illuminate\Http\Request;

class PenerbitController extends Controller
{
    public function index()
    {

        // $p = PenerbitModel::all();
        $p = DB::table('penerbit')
            ->where('is_active', '1')
            ->get();

        return view('admin.penerbit.index', compact('p'));

    }

    public function create_data()
    {

        return view('admin.penerbit.tambah');
    }

    public function postData(Request $request, PenerbitModel $PenerbitModel)
    {

        $simpan = $PenerbitModel->create([
            'id_penerbit' => $request->id_penerbit,
            'nama_penerbit' => $request->nama_penerbit,
            'no_telp' => $request->no_telp,
            'alamat_penerbit' => $request->alamat_penerbit,
            'is_active' => $request->is_active,
        ]);

        if (!$simpan->exists) {
            return redirect()->route('tampil_penerbit')->with('error', 'data gagal disimpan');
        }
        return redirect()->route('tampil_penerbit')->with('success', 'data berhasil disimpan');
    }

    public function editData($id)
    {
        $data = PenerbitModel::where('id_penerbit', $id)->first();
        return view('admin.penerbit.edit', compact('data'));
    }

    public function updateData($id, PenerbitModel $PenerbitModel, Request $request)
    {

        $simpan = $PenerbitModel->where('id_penerbit', $id)->update([
            'nama_penerbit' => $request->nama_penerbit,
            'no_telp' => $request->no_telp,
            'alamat_penerbit' => $request->alamat_penerbit,
            'is_active' => $request->is_active,
            'updated_at' => now(),
        ]);

        if (!$simpan) {
            return redirect()->route('tampil_penerbit')->with('error', 'data gagal di update');
        }

        return redirect()->route('tampil_penerbit')->with('success', 'data berhasil di update');
    }

    public function softDelete($id, PenerbitModel $PenerbitModel)
    {

        $simpan = $PenerbitModel->where('id_penerbit', $id)->update([
            'is_active' => '0',
        ]);

        if (!$simpan) {
            return redirect()->route('tampil_penerbit')->with('error', 'data gagal di dihapus');
        }

        return redirect()->route('tampil_penerbit')->with('success', 'data berhasil di hapus');
    }

}