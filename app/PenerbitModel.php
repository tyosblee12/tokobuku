<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PenerbitModel extends Model
{
    protected $table = 'penerbit';
    protected $fillable = [
        'id_penerbit', 'nama_penerbit', 'no_telp', 'is_active', 'alamat_penerbit', 'created_at', 'updated_at',
    ];
}