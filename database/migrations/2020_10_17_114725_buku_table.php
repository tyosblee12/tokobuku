<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BukuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buku', function (Blueprint $table) {
            $table->bigIncrements('id_buku');
            $table->bigInteger('id_penerbit')->unsigned();
            $table->foreign('id_penerbit')->references('id_penerbit')->on('penerbit');
            $table->string('nama_buku', 50);
            $table->integer('is_active');
            $table->integer('stok');
            $table->integer('tahun_terbit');
            $table->date('created_at');
            $table->date('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buku');
    }
}