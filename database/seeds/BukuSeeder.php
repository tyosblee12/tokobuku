<?php

use Illuminate\Database\Seeder;

class BukuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('buku')->insert(array(
            array(
                'id_buku'       => '101',
                'nama_buku'     => 'Bahasa Indonesia',
                'stok'          => 10,
                'tahun_terbit'  => 2010,
                'id_penerbit'   => 201,
                'is_active'     => 1,
                'created_at'   => now(),
                'updated_at'   => now(),
                ),
            array(
                'id_buku'   => '102',
                'nama_buku'  => 'Bahasa Inggris',
                'stok'          => 15,
                'tahun_terbit'  => 2011,
                'id_penerbit'    => 202,
                'is_active'     => 1,
                'created_at'    => now(),
                'updated_at'    => now(),
                ),
            array(
                'id_buku'   => '103',
                'nama_buku'  => 'Sejarah',
                'stok'          => 15,
                'tahun_terbit'  => 2012,
                'id_penerbit'    => 203,
                'is_active'     => 1,
                'created_at'    => now(),
                'updated_at'    => now(),
                ),
            array(
                'id_buku'   => '104',
                'nama_buku'  => 'Mahir Photoshop',
                'stok'          => 15,
                'tahun_terbit'  => 2013,
                'id_penerbit'    => 204,
                'is_active'     => 1,
                'created_at'    => now(),
                'updated_at'    => now(),
                ),
        ));
    }
}