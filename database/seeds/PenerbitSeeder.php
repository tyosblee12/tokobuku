<?php

use Illuminate\Database\Seeder;

class PenerbitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $penerbit = [
            [
                'id_penerbit' => 201,
                'nama_penerbit' => 'Gramedia',
                'no_telp' => 628511330001,
                'is_active' => 1,
                'alamat_penerbit' => 'Cimahi',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id_penerbit' => 202,
                'nama_penerbit' => 'Erlangga',
                'no_telp' => 6282115330000,
                'is_active' => 1,
                'alamat_penerbit' => 'Sukabumi',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id_penerbit' => 203,
                'nama_penerbit' => 'Grasindo',
                'no_telp' => 6282115330000,
                'is_active' => 1,
                'alamat_penerbit' => 'Bogor',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id_penerbit' => 204,
                'nama_penerbit' => 'Gagas Media',
                'no_telp' => 6285315330000,
                'is_active' => 1,
                'alamat_penerbit' => 'Depok',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id_penerbit' => 205,
                'nama_penerbit' => 'Elexmedia',
                'no_telp' => 6282225330000,
                'is_active' => 1,
                'alamat_penerbit' => 'Bandung',
                'created_at' => now(),
                'updated_at' => now(),
            ],

        ];

        DB::table('penerbit')->insert($penerbit);
    }
}