@extends('admin.layouts.master')
@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h5>Tambah Data Buku</h5>
        </div>
        <div class="card-body">
            <h5>Form Data Buku</h5>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <form action="{{ route('update_data',$data->id_buku) }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="Nama Buku">Nama Buku</label>
                            <input type="text" class="form-control" name="nama_buku" require="nama_buku" id="nama_buku"
                                placeholder="Masukan Nama Buku" value="{{ $data->nama_buku }}">
                        </div>
                        <div class="form-group">
                            <label for="Stok">Stok per Lusin</label>
                            <input type="number" class="form-control" name="stok" require="stok" id="stok"
                                placeholder="Stok" value="{{ $data->stok }}">
                        </div>
                        <div class="form-group">
                            <label for="Stok">Tahun Tertit</label>
                            <select class="form-control" id="tahun_terbit" required="tahun_terbit" name="tahun_terbit">
                                <option value="{{ $data->tahun_terbit }}" set>{{ $data->tahun_terbit }}</option>
                                @foreach(range(date('Y')-20, date('Y')) as $y)
                                <option value="{{ $y }}">{{$y}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class=" form-group">
                            <label for="ID Penerbit">ID Penerbit</label>
                            <select class="form-control" id="id_penerbit" name="id_penerbit">
                                @foreach($p as $row)
                                <option value="{{ $row-> id_penerbit }}">{{ $row-> id_penerbit }} -
                                    {{ $row-> nama_penerbit }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary float-right mt-3">Simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection