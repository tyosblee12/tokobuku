@extends('admin.layouts.master')
@section('content')

<div class="col-xl-12">
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Warehouse of Book </h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#"><i class="feather icon-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#!">Tables</a></li>
                        <li class="breadcrumb-item"><a href="javascript:">Table Buku</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h5>Tabel Buku</h5>
            <span class="d-block m-t-5">use class <code>table</code> inside table element</span>
        </div>
        <form type="GET" action="{{ route ('cari_buku') }}">
            <div class="input-group input-group-sm mb-3 col-lg-12">
                <a href="{{ route ('tambah_buku') }}" title="Tambah Data" class="btn btn-primary btn-sm text-white"><i
                        class="fas fa-plus-circle"></i>Tambah Data</a>
                <input type="search" class="form-control" placeholder="Cari Nama Buku ..." name="cari" aria-label="cari"
                    aria-describedby="basic-addon2">
                <div class="input-group-append">
                    <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i> Cari</button>
                </div>
            </div>
        </form>
        <div class="card-block table-border-style">
            <div class="table-responsive">
                <table class="table table-hover font-weight-bold">
                    <thead class="bg-dark text-white">
                        <tr>
                            <th>No</th>
                            <th>ID Buku</th>
                            <th>Nama Buku</th>
                            <th>Stok</th>
                            <th>Buah</th>
                            <th>Tahun Terbit</th>
                            <th>ID Penerbit</th>
                            <th>Nama Penerbit</th>
                            <th>Tanggal Input</th>
                            <th>Tanggal Update</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    @php

                    $i=1;
                    $pcs=12;

                    @endphp
                    @foreach($data as $row)
                    <tbody>
                        <tr>
                            <th scope="row">{{ $i++ }}</th>
                            <td>{{ $row-> id_buku }} </td>
                            <td>{{ $row-> nama_buku }} </td>
                            <td>{{ $row-> stok }} Lusin</td>
                            <td>{{ $row-> stok*$pcs }} Pcs</td>
                            <td>{{ $row-> tahun_terbit }} </td>
                            <td>{{ $row-> id_penerbit }} </td>
                            <td>{{ $row-> nama_penerbit }} </td>
                            <td>{{ $row-> created_at }} </td>
                            <td>{{ $row-> updated_at }} </td>
                            <td>
                                <a href="{{route('edit_data', $row->id_buku)}}" title="Ubah"
                                    class="label btn-outline-primary btn-sm text-primary "><i class="fa fa-edit"></i>
                                    Ubah</a>
                                <a href="{{route('hapus_data', $row->id_buku)}}" title="Hapus"
                                    class="label btn-outline-danger btn-sm text-danger"><i class="fa fa-trash-alt"></i>
                                    Hapus</a>
                            </td>
                        </tr>
                    </tbody>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>
@endsection