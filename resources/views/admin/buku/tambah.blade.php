@extends('admin.layouts.master')
@section('content')

<!-- [ basic-table ] start -->
<div class="col-xl-6">
    <div class="card">
        <div class="card-header">
            <h5>Form Tambah Data Buku</h5>
            <span class="d-block m-t-5">use class <code>table</code> inside table element</span>
        </div>
        <div class="card-block table-border-style">
            <form action="{{ route ('post_buku') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="Nama Buku">Nama Buku</label>
                    <input type="text" class="form-control" name="nama_buku" require="nama_buku" id="nama_buku"
                        placeholder="Masukan Nama Buku">
                </div>
                <div class="form-group">
                    <label for="Stok">Stok</label>
                    <input type="number" class="form-control" name="stok" require="stok" id="stok" placeholder="Stok">
                </div>
                <div class="form-group">
                    <label for="tahun_terbit">Tahun Terbit</label>
                    <select class="form-control" name="tahun_terbit" require="tahun_terbit" id="tahun_terbit">
                        <option set>Pilih Tahun Terbit </option>
                        @foreach(range(date('Y')-20, date('Y')) as $y)
                        <option value="{{ $y }}">{{$y}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="ID Penerbit">ID Penerbit</label>
                    <select class="form-control" id="id_penerbit" name="id_penerbit">
                        <option set>Pilih ID Penerbit</option>
                        @foreach($p as $row)
                        <option value="{{ $row-> id_penerbit }}">{{ $row-> id_penerbit }} -
                            {{ $row-> nama_penerbit }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="is_active">Show/Hide</label>
                    <select class="form-control" id="is_active" name="is_active">
                        <option value="1">Show</option>
                        <option value="0">Hide</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="Created At">Tanggal Input</label>
                    <input type="date" class="form-control" name="created_at" require="created_at" id="created_at"
                        placeholder="Tanggal Input">
                </div>
                <a type="button" href="{{ route ('tampil_buku')}}"
                    class="btn btn-secondary btn-sm text-white float-right mt-3">Kembali</a>
                <button type="submit" class="btn btn-primary float-right mt-3">Simpan</button>
            </form>
        </div>
    </div>
</div>
<!-- [ basic-table ] end -->

<!-- [ Hover-table ] start -->
<div class="col-xl-6">
    <div class="card">
        <div class="card-header">
            <h5>Form Tabel</h5>
            <span class="d-block m-t-5">use class <code>table-hover</code> inside table element</span>
        </div>
        <div class="card-block table-border-style">
            <div class="table-responsive">

            </div>
        </div>
    </div>
</div>
<!-- [ Hover-table ] end -->

@endsection