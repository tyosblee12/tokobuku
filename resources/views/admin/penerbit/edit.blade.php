@extends('admin.layouts.master')
@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h5>Edit Data Penerbit</h5>
        </div>
        <div class="card-body">
            <h5>Form Data Penerbit</h5>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <form action="{{route('update_penerbit',$data->id_penerbit)}}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="Nama Buku">Nama Penerbit</label>
                            <input type="text" class="form-control" name="nama_penerbit" require="nama_penerbit"
                                id="nama_penerbit" placeholder="Masukan Nama Buku" value="{{ $data->nama_penerbit }}">
                        </div>
                        <div class="form-group">
                            <label for="Nomor Telepon">Nomor Telepon</label>
                            <input type="text" class="form-control" name="no_telp" require="no_telp" id="no_telp"
                                placeholder="Masukan Nomor Telp" Value="{{ $data->no_telp }}">
                        </div>
                        <div class="form-group">
                            <label for="is_active">Alamat Penerbit</label>
                            <textarea type="textarea" class="form-control" name="alamat_penerbit"
                                require="alamat_penerbit" id="alamat_penerbit" placeholder="Alamat Penerbit"
                                Value="{{ $data->alamat_penerbit }}">{{ $data->alamat_penerbit }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="is_active">Show/Hide</label>
                            <select class="form-control" id="is_active" require="is_active" name="is_active">
                                <option value="{{ $data->is_active }}" set>{{ $data->is_active }}</option>
                                <option value="0">0</option>
                            </select>
                        </div>
                        <a type="button" href="#"
                            class="btn btn-secondary btn-sm text-white float-right mt-3">Kembali</a>
                        <button type="submit" class="btn btn-primary float-right mt-3">Simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection