@extends('admin.layouts.master')
@section('content')

<div class="col-xl-12">
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Bootstrap Basic Tables</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#"><i class="feather icon-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#!">Tables</a></li>
                        <li class="breadcrumb-item"><a href="javascript:">Tabel Penerbit</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h5>Tabel Penerbit</h5>
            <span class="d-block m-t-5">use class <code>table</code> inside table element</span>
        </div>
        <div class="card-block table-border-style">
            <div class="table-responsive">
                <a href="{{route ('tambah_penerbit')}}" title="Tambah Data" class="btn btn-primary btn-sm text-white"><i
                        class="fas fa-plus-circle"></i>
                    Tambah Data </a>
                <table class="table table-hover font-weight-bold">
                    <thead class="bg-dark text-white">
                        <tr>
                            <th>No</th>
                            <th>ID Penerbit</th>
                            <th>Nama Penerbit</th>
                            <th>Nomor Telepon</th>
                            <th>Alamat</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    @php
                    $i=1;
                    @endphp
                    @foreach($p as $row)
                    <tbody>
                        <tr>
                            <th scope="row">{{ $i++ }}</th>
                            <td>{{ $row-> id_penerbit }} </td>
                            <td>{{ $row-> nama_penerbit }} </td>
                            <td>+{{ $row-> no_telp }} </td>
                            <td class="row-md-4">{{ $row-> alamat_penerbit }} </td>
                            <td>
                                <a href="{{route ('edit_penerbit', $row->id_penerbit) }}" title="Ubah"
                                    class="label btn-outline-primary btn-sm text-primary ">
                                    <i class="fa fa-edit"></i>Ubah</a>

                                <a href="{{route ('hapus_data', $row->id_penerbit)}}" title="Hapus"
                                    class="label btn-outline-danger btn-sm text-danger">
                                    <i class="fa fa-trash-alt"></i>Hapus</a>
                            </td>
                        </tr>
                    </tbody>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>


@endsection