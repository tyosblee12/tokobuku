@extends('admin.layouts.master')
@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h5>Tambah Data Penerbit</h5>
        </div>
        <div class="card-body">
            <h5>Form Data Penerbit</h5>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <form action="{{route('post_penerbit')}}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="Nama Buku">Nama Penerbit</label>
                            <input type="text" class="form-control" name="nama_penerbit" require="nama_penerbit"
                                id="nama_penerbit" placeholder="Nama Penerbit">
                        </div>
                        <div class="form-group">
                            <label for="Nomor Telepon">Nomor Telepon</label>
                            <input type="number" class="form-control" name="no_telp" require="no_telp" id="no_telp"
                                placeholder="Masukan Nomor Telp" Value="62">
                        </div>
                        <div class="form-group">
                            <label for="Nomor Telepon">Alamat</label>
                            <textarea type="text" class="form-control" name="alamat_penerbit" require="alamat_penerbit"
                                id="alamat_penerbit" placeholder="Alamat"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="is_active">Show/Hide</label>
                            <select class="form-control" id="is_active" name="is_active">
                                <option set>Pilih ... </option>
                                <option value="0">0</option>
                                <option value="1">1</option>
                            </select>
                        </div>
                        <a type="button" href="{{ route ('tampil_penerbit')}}"
                            class="btn btn-secondary btn-sm text-white float-right mt-3">Kembali</a>
                        <button type="submit" class="btn btn-primary float-right mt-3">Simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection