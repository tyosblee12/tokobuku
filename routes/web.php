<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('admin.dashboard');
})->name('dashboard');

// BUKU ROUTE

Route::get('/buku', 'BukuController@index')->name('tampil_buku');
Route::get('/tbuku', 'BukuController@tambah_buku')->name('tambah_buku');
Route::post('/pbuku', 'BukuController@postData1')->name('post_buku');

Route::get('ebuku/{id}', 'BukuController@editData')->name('edit_data');
Route::post('ubuku/{id}', 'BukuController@updateData')->name('update_data');

Route::get('/hbuku{id}', 'BukuController@softDelete')->name('delete_data');
Route::get('/cbuku', 'BukuController@search')->name('cari_buku');

// PENERBIT ROUTE

Route::get('/penerbit', 'PenerbitController@index')->name('tampil_penerbit');
Route::get('/tpenerbit', 'PenerbitController@create_data')->name('tambah_penerbit');
Route::post('/ppenerbit', 'PenerbitController@postData')->name('post_penerbit');

Route::get('epenerbit/{id}', 'PenerbitController@editData')->name('edit_penerbit');
Route::post('upenerbit/{id}', 'PenerbitController@updateData')->name('update_penerbit');
Route::post('upenerbit/{id}', 'PenerbitController@updateData')->name('update_penerbit');
Route::get('/hpenerbit{id}', 'PenerbitController@softDelete')->name('hapus_data');